//
//  MKBaseViewController.swift
//  KorsContacts
//
//  Created by OSX on 11/14/19.
//  Copyright © 2019 David Vallas. All rights reserved.
//

import Foundation

class MKBaseViewController: UIViewController {
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        
        if #available(iOS 13.0, *) {
            viewControllerToPresent.modalPresentationStyle = .overFullScreen
        }
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}
