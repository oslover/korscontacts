//
//  CommentTransitioningDelegate.swift
//  TabletAssignmentSwift
//
//  Created by David Vallas on 4/9/16.


import UIKit

class BottomMenuTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    var chromeBackgroundAlpha: CGFloat = 0.4
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = BottomMenuPresentationController(
            presentedViewController:presented,
            presenting:presenting,
            chromeBackgroundAlpha: chromeBackgroundAlpha)
        return presentationController
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = BottomMenuAnimatedTransitioning()
        animationController.isPresentation = true
        return animationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animationController = BottomMenuAnimatedTransitioning()
        animationController.isPresentation = false
        return animationController
    }
}
