//
//  CommentPresentationController.swift
//  TabletAssignmentSwift
//
//  Created by David Vallas on 4/9/16.


import UIKit

class BottomMenuPresentationController: UIPresentationController, UIAdaptivePresentationControllerDelegate {
    
    var chromeView: UIView = UIView()
    
    let viewPercent = CGSize(width: 0.5, height: 0.4)
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }
    
    init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, chromeBackgroundAlpha: CGFloat) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        setupChrome(withAlpha: chromeBackgroundAlpha)
    }
    
    func setupChrome(withAlpha: CGFloat) {
        chromeView.backgroundColor = UIColor(white:0.0, alpha:withAlpha)
        chromeView.alpha = 0.0
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.chromeViewTapped))
        chromeView.addGestureRecognizer(tap)
    }
    
    @objc func chromeViewTapped(gesture: UIGestureRecognizer) {
        if (gesture.state == UIGestureRecognizerState.ended) {
            presentingViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        var presentedViewFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
        let containerBounds = containerView!.bounds
        presentedViewFrame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerBounds.size)
        presentedViewFrame.origin.y = (chromeView.frame.height - presentedViewFrame.size.height) / 2.0
        presentedViewFrame.origin.x = (chromeView.frame.width - presentedViewFrame.size.width) / 2.0
        return presentedViewFrame
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        let size = CGSize(width: parentSize.width * viewPercent.width, height: parentSize.height * viewPercent.height)
        return size
    }
    
    override func presentationTransitionWillBegin() {
        chromeView.frame = self.containerView!.bounds
        chromeView.alpha = 0.0
        containerView!.insertSubview(chromeView, at:0)
        let coordinator = presentedViewController.transitionCoordinator
        if (coordinator != nil) {
            coordinator!.animate(alongsideTransition: {
                (context:UIViewControllerTransitionCoordinatorContext!) -> Void in
                self.chromeView.alpha = 1.0
                self.presentedView?.alpha = 1.0
                }, completion:nil)
        } else {
            chromeView.alpha = 1.0
            self.presentedView?.alpha = 1.0
        }
    }
    
    override func dismissalTransitionWillBegin() {
        let coordinator = presentedViewController.transitionCoordinator
        if (coordinator != nil) {
            coordinator!.animate(alongsideTransition: {
                (context:UIViewControllerTransitionCoordinatorContext!) -> Void in
                self.chromeView.alpha = 0.0
                self.presentedView?.alpha = 0.0
                }, completion:nil)
        } else {
            chromeView.alpha = 0.0
            self.presentedView?.alpha = 0.0
        }
    }
    
    override func containerViewWillLayoutSubviews() {
        chromeView.frame = containerView!.bounds
        presentedView!.frame = frameOfPresentedViewInContainerView
    }
    
}
