//
//  MKGender.swift
//  KorsContacts
//
//  Created by David Vallas on 4/12/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation

enum MKGender: String {
    case male = "Male"
    case female = "Female"
    
    static func genderString(forGender: MKGender?) -> String {
        if let g = forGender { return g.rawValue }
        return "Unknown"
    }
    
    var saveGender: String {
        
        switch self {
        case .male: return "M"
        case .female: return "F"
        }
        
    }
}
