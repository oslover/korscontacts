//
//  MKCountry.swift
//  KorsContacts
//
//  Created by David Vallas on 4/4/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation

struct MKCountry {
    
    let code: String
    let name: String
    
    static func country(withText: String) -> MKCountry {
        
        let countries = MKCountry.countries
        
        for c in countries {
            if c.name.contains(withText) {
                return c
            }
        }
        
        return countries.first!
    }
    
    static var countries: [MKCountry] {
        
        var countries: [MKCountry] = []
        let l = MKLanguage.getSaved()
        let current = NSLocale.current.regionCode
        let language = l.languageIdentifer
        
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: language).displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            let mkcountry = MKCountry(code: code, name: name)
            if code == current {
                countries.insert(mkcountry, at: 0)
            } else {
                countries.append(mkcountry)
            }
        }
        
        return countries
    }
    
    
    var iso3Code: String {
        switch self.code {
        case "AF": return "AFG"
        case "AL": return "ALB"
        case "DZ": return "DZA"
        case "AS": return "ASM"
        case "AD": return "AND"
        case "AO": return "AGO"
        case "AI": return "AIA"
        case "AQ": return "ATA"
        case "AG": return "ATG"
        case "AR": return "ARG"
        case "AM": return "ARM"
        case "AW": return "ABW"
        case "AU": return "AUS"
        case "AT": return "AUT"
        case "AZ": return "AZE"
        case "BS": return "BHS"
        case "BH": return "BHR"
        case "BD": return "BGD"
        case "BB": return "BRB"
        case "BY": return "BLR"
        case "BE": return "BEL"
        case "BZ": return "BLZ"
        case "BJ": return "BEN"
        case "BM": return "BMU"
        case "BT": return "BTN"
        case "BO": return "BOL"
        case "BA": return "BIH"
        case "BW": return "BWA"
        case "BV": return "BVT"
        case "BR": return "BRA"
        case "IO": return "IOT"
        case "VG": return "VGB"
        case "BN": return "BRN"
        case "BG": return "BGR"
        case "BF": return "BFA"
        case "BI": return "BDI"
        case "KH": return "KHM"
        case "CM": return "CMR"
        case "CA": return "CAN"
        case "CV": return "CPV"
        case "KY": return "CYM"
        case "CF": return "CAF"
        case "TD": return "TCD"
        case "CL": return "CHL"
        case "CN": return "CHN"
        case "CX": return "CXR"
        case "CC": return "CCK"
        case "CO": return "COL"
        case "KM": return "COM"
        case "CD": return "COD"
        case "CG": return "COG"
        case "CK": return "COK"
        case "CR": return "CRI"
        case "CI": return "CIV"
        case "CU": return "CUB"
        case "CY": return "CYP"
        case "CZ": return "CZE"
        case "DK": return "DNK"
        case "DJ": return "DJI"
        case "DM": return "DMA"
        case "DO": return "DOM"
        case "EC": return "ECU"
        case "EG": return "EGY"
        case "SV": return "SLV"
        case "GQ": return "GNQ"
        case "ER": return "ERI"
        case "EE": return "EST"
        case "ET": return "ETH"
        case "FO": return "FRO"
        case "FK": return "FLK"
        case "FJ": return "FJI"
        case "FI": return "FIN"
        case "FR": return "FRA"
        case "GF": return "GUF"
        case "PF": return "PYF"
        case "TF": return "ATF"
        case "GA": return "GAB"
        case "GM": return "GMB"
        case "GE": return "GEO"
        case "DE": return "DEU"
        case "GH": return "GHA"
        case "GI": return "GIB"
        case "GR": return "GRC"
        case "GL": return "GRL"
        case "GD": return "GRD"
        case "GP": return "GLP"
        case "GU": return "GUM"
        case "GT": return "GTM"
        case "GN": return "GIN"
        case "GW": return "GNB"
        case "GY": return "GUY"
        case "HT": return "HTI"
        case "HM": return "HMD"
        case "VA": return "VAT"
        case "HN": return "HND"
        case "HK": return "HKG"
        case "HR": return "HRV"
        case "HU": return "HUN"
        case "IS": return "ISL"
        case "IN": return "IND"
        case "ID": return "IDN"
        case "IR": return "IRN"
        case "IQ": return "IRQ"
        case "IE": return "IRL"
        case "IL": return "ISR"
        case "IT": return "ITA"
        case "JM": return "JAM"
        case "JP": return "JPN"
        case "JO": return "JOR"
        case "KZ": return "KAZ"
        case "KE": return "KEN"
        case "KI": return "KIR"
        case "KP": return "PRK"
        case "KR": return "KOR"
        case "KW": return "KWT"
        case "KG": return "KGZ"
        case "LA": return "LAO"
        case "LV": return "LVA"
        case "LB": return "LBN"
        case "LS": return "LSO"
        case "LR": return "LBR"
        case "LY": return "LBY"
        case "LI": return "LIE"
        case "LT": return "LTU"
        case "LU": return "LUX"
        case "MO": return "MAC"
        case "MK": return "MKD"
        case "MG": return "MDG"
        case "MW": return "MWI"
        case "MY": return "MYS"
        case "MV": return "MDV"
        case "ML": return "MLI"
        case "MT": return "MLT"
        case "MH": return "MHL"
        case "MQ": return "MTQ"
        case "MR": return "MRT"
        case "MU": return "MUS"
        case "YT": return "MYT"
        case "MX": return "MEX"
        case "FM": return "FSM"
        case "MD": return "MDA"
        case "MC": return "MCO"
        case "MN": return "MNG"
        case "MS": return "MSR"
        case "MA": return "MAR"
        case "MZ": return "MOZ"
        case "MM": return "MMR"
        case "NA": return "NAM"
        case "NR": return "NRU"
        case "NP": return "NPL"
        case "AN": return "ANT"
        case "NL": return "NLD"
        case "NC": return "NCL"
        case "NZ": return "NZL"
        case "NI": return "NIC"
        case "NE": return "NER"
        case "NG": return "NGA"
        case "NU": return "NIU"
        case "NF": return "NFK"
        case "MP": return "MNP"
        case "NO": return "NOR"
        case "OM": return "OMN"
        case "PK": return "PAK"
        case "PW": return "PLW"
        case "PS": return "PSE"
        case "PA": return "PAN"
        case "PG": return "PNG"
        case "PY": return "PRY"
        case "PE": return "PER"
        case "PH": return "PHL"
        case "PN": return "PCN"
        case "PL": return "POL"
        case "PT": return "PRT"
        case "PR": return "PRI"
        case "QA": return "QAT"
        case "RE": return "REU"
        case "RO": return "ROU"
        case "RU": return "RUS"
        case "RW": return "RWA"
        case "SH": return "SHN"
        case "KN": return "KNA"
        case "LC": return "LCA"
        case "PM": return "SPM"
        case "VC": return "VCT"
        case "WS": return "WSM"
        case "SM": return "SMR"
        case "ST": return "STP"
        case "SA": return "SAU"
        case "SN": return "SEN"
        case "CS": return "SCG"
        case "SC": return "SYC"
        case "SL": return "SLE"
        case "SG": return "SGP"
        case "SK": return "SVK"
        case "SI": return "SVN"
        case "SB": return "SLB"
        case "SO": return "SOM"
        case "ZA": return "ZAF"
        case "GS": return "SGS"
        case "ES": return "ESP"
        case "LK": return "LKA"
        case "SD": return "SDN"
        case "SR": return "SUR"
        case "SJ": return "SJM"
        case "SZ": return "SWZ"
        case "SE": return "SWE"
        case "CH": return "CHE"
        case "SY": return "SYR"
        case "TW": return "TWN"
        case "TJ": return "TJK"
        case "TZ": return "TZA"
        case "TH": return "THA"
        case "TL": return "TLS"
        case "TG": return "TGO"
        case "TK": return "TKL"
        case "TO": return "TON"
        case "TT": return "TTO"
        case "TN": return "TUN"
        case "TR": return "TUR"
        case "TM": return "TKM"
        case "TC": return "TCA"
        case "TV": return "TUV"
        case "VI": return "VIR"
        case "UG": return "UGA"
        case "UA": return "UKR"
        case "AE": return "ARE"
        case "GB": return "GBR"
        case "UM": return "UMI"
        case "US": return "USA"
        case "UY": return "URY"
        case "UZ": return "UZB"
        case "VU": return "VUT"
        case "VE": return "VEN"
        case "VN": return "VNM"
        case "WF": return "WLF"
        case "EH": return "ESH"
        case "YE": return "YEM"
        case "ZM": return "ZMB"
        case "ZW": return "ZWE"
        default: return "USA"
        }
    }
    
}
