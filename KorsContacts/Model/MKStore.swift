//
//  MKStore.swift
//  KorsContacts
//
//  Created by David Vallas on 4/24/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation

var primaryStore: MKStore = MKStore(id: "9999", address: "UNKNOWN", address2: "UNKNOWN", city: "UNKNOWN")

struct MKStore {
    
    let id: String
    let address: String
    let address2: String
    let city: String

    static func parseJSON(json: Dictionary<String, AnyObject>) -> [MKStore] {
        
        var stores: [MKStore] = []
        
        let response = json["response"] as? Dictionary ?? [:]
        let dictionaries = response["locations"] as? [Dictionary] ?? [[:]]
        
        for dict in dictionaries {
            let id = dict["id"].string
            let address = dict["address"].string
            let address2 = dict["address2"].optionalString
            let city = dict["city"].string
            let store = MKStore(id: id, address: address, address2: address2, city: city)
            stores.append(store)
        }

        return stores
    }
    
}
