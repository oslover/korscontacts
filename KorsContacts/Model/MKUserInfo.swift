//
//  MKUserInfo.swift
//  KorsContacts
//
//  Created by David Vallas on 4/17/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation

struct MKUserInfo {
    let firstName: String
    let lastName: String
    let email: String
    let country: MKCountry
    let phoneNumber: String?
    let gender: MKGender?
    let birthdate: String?
    let collection: Bool
    let men: Bool
    let vip: Bool
    let signMeUp: Bool
}
