//
//  MKCustomer.swift
//  KorsContacts
//
//  Created by David Vallas on 2/2/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import Foundation

struct MKCustomer {
    
    let customerid: String
    let email: String
    let firstName: String
    let lastName: String
    let gender: String
    let birthdate: String
    
    init(customerid i: String, email e: String, firstName f: String, lastName l: String, gender g: String, birthdate b: String) {
        customerid = i
        email = e
        firstName = f
        lastName = l
        gender = g
        birthdate = b
    }
}
