//
//  MKSignupVC.swift
//  KorsContacts
//
//  Created by David Vallas on 4/4/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

class MKSignupVC: MKBaseViewController {
    
    // MARK: - Outlets
    
    fileprivate var popupTransitionDelegate: DVTransDelegate!
    fileprivate var ssTransitionDelegate: DVTransDelegate!
    fileprivate var timer: Timer?
    
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var birthdateTF: UITextField!
    
    @IBOutlet weak var requiredLabel: UILabel!
    @IBOutlet weak var neverMissABeatLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var birthdateLabel: UILabel!
    @IBOutlet weak var hearAboutTV: UITextView!
    @IBOutlet weak var collectionToggle: CheckButton!
    @IBOutlet weak var collectionTV: UITextView!
    @IBOutlet weak var menToggle: CheckButton!
    @IBOutlet weak var menTV: UITextView!
    @IBOutlet weak var vipToggle: CheckButton!
    @IBOutlet weak var vipTV: UITextView!
    @IBOutlet weak var signupToggle: CheckButton!
    @IBOutlet weak var signupTV: UITextView!
    @IBOutlet weak var disclaimerTV: UITextView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var selectAStoreButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var beInKnowLabel: UILabel!
    
    @IBOutlet weak var phoneSpacer: NSLayoutConstraint!
    @IBOutlet weak var phone2Spacer: NSLayoutConstraint!
    @IBOutlet weak var phoneLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var phoneTFHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionToggleHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionTVHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionSpacer: NSLayoutConstraint!
    @IBOutlet weak var menToggleHeight: NSLayoutConstraint!
    @IBOutlet weak var menTVHeight: NSLayoutConstraint!
    @IBOutlet weak var menSpacer: NSLayoutConstraint!
    @IBOutlet weak var vipToggleHeight: NSLayoutConstraint!
    @IBOutlet weak var vipTVHeight: NSLayoutConstraint!
    @IBOutlet weak var vipSpacer: NSLayoutConstraint!
    
    // MARK: - Variables
    
    fileprivate var local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
    var email: String = ""
    fileprivate var country = MKCountry.countries.first!
    fileprivate var gender: MKGender?
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPopupTransitionDelegate()
        setSSTransitionDelegate()
        setDefaultDisplay()
        observeTextFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearFields()
        setDefaultDisplay()
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Signup-\(primaryStore.id)")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let yPos = signupButton.frame.origin.y * 0.6
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: yPos + scrollView.frame.height * 1.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timer?.invalidate()
        setTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        clearFields()
        timer?.invalidate()
    }
    
    deinit {
        timer?.invalidate()
    }
    
    // MARK: - Button Actions
    
    @IBAction func languagePressed(_ sender: UIButton) {
        timer?.invalidate()
        setTimer()
        let sb = UIStoryboard(name: "Popup", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MKLanguagePicker") as! MKLanguagePicker
        vc.delegate = self
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = popupTransitionDelegate
        present(vc, animated: true) {}
    }
    
    @IBAction func selectAStorePressed(_ sender: UIButton) {
        displaySelectAStore()
    }
    
    
    @IBAction func fieldEdited(_ sender: UITextField) {
        timer?.invalidate()
        setTimer()
    }
    
    @IBAction func signupPressed(_ sender: UIButton) {
        
        timer?.invalidate()
        setTimer()
        
        if isReadyForSignup {
            
            let phoneNumber = phoneNumberTF.text == "" ? nil : phoneNumberTF.text
            let birthdate = birthdateTF.text == "" ? nil : birthdateTF.text
            
            let user = MKUserInfo(firstName: firstNameTF.text!,
                                  lastName: lastNameTF.text!,
                                  email: emailTF.text!,
                                  country: country,
                                  phoneNumber: phoneNumber,
                                  gender: gender,
                                  birthdate: birthdate,
                                  collection: collectionToggle.isOn,
                                  men: menToggle.isOn,
                                  vip: vipToggle.isOn,
                                  signMeUp: signupToggle.isOn)
            
            startIndicator()
            
            MKNetwork.shared.saveCustomer(withUserInfo: user, completion: { [weak self] (response, error) in
                self?.stopIndicator()
                
                if error != nil {
                    self?.displayAlert(withError: error!)
                    return
                } else {
                    if self?.vipToggle.isOn != false {
                        let bd = (self?.birthdateTF.text == "" || self?.birthdateTF.text == nil) ? "01/01" : self!.birthdateTF.text!
                        let customer = MKCustomer(customerid: response.customerNumber,
                                                      email: self?.emailTF.text! ?? "",
                                                      firstName: self?.firstNameTF.text! ?? "",
                                                      lastName: self?.lastNameTF.text! ?? "",
                                                      gender: MKGender.genderString(forGender: self?.gender),
                                                      birthdate: bd)
                        
                        MKNetwork.shared.setVIP(withCustomer: customer, completion: { (results, error) in
                            
                            if error != nil {
                                self?.displayAlert(withError: error!)
                            } else {
                                self?.displayCompletion()
                            }
                        })
                        
                    } else {
                        self?.displayCompletion()
                    }
                }
            })
            
        }
    }
    
    // MARK: - Targets
    
    @objc func textFieldDidBegin(_ textField: UITextField) {
        let noFonts = local.languageText == "言語" // we dont want fonts for korean
        textField.format1(color: UIColor.black, borderColorBlack: true, noFonts: noFonts)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let noFonts = local.languageText == "言語" // we dont want fonts for korean
        textField.format1(color: UIColor.black, borderColorBlack: true, noFonts: noFonts)
    }
    
    @objc func textFieldDidEnd(_ textField: UITextField) {
        let noFonts = local.languageText == "言語" // we dont want fonts for korean
        textField.format1(color: UIColor.black, borderColorBlack: false, noFonts: noFonts)
    }
    
    @objc func displayScreenSaver() {
        if presentedViewController == nil {
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "MKScreenSaver") as! MKScreenSaver
            vc.delegate = self
            vc.modalPresentationStyle = .custom
            vc.transitioningDelegate = ssTransitionDelegate
            present(vc, animated: true) {}
        } else {
            timer?.invalidate()
            setTimer()
        }
    }
    
    // MARK: - Private Functions
    
    fileprivate func displayAlert(withError e: Error) {
        let alert = UIAlertController(title: local.alert, message: e.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    fileprivate func displayCompletion() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MKCompletionVC") as! MKCompletionVC
        vc.emailAddress = emailTF.text ?? ""
        present(vc, animated: true) {}
    }
    
    fileprivate func displaySelectAStore() {
        dismiss(animated: true) {}
    }
    
    fileprivate func setTimer() {
        timer = Timer.scheduledTimer(timeInterval: 90, target: self, selector: #selector(displayScreenSaver), userInfo: nil, repeats: false)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        timer?.invalidate()
        setTimer()
    }
    
    fileprivate func setDefaultDisplay() {
        collectionToggle.isOn = false
        menToggle.isOn = false
        signupToggle.isOn = false
        vipToggle.isOn = false
        stopIndicator()
        setBackgroundTransparency()
        updateTextAndSizes()
    }
    
    fileprivate func updateTextAndSizes() {
        local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
        setupBeInKnow()
        setupLabelTextsAndSizes()
        setupDisclaimer()
        setupToggleViews()
        setupFormats()
    }
    
    fileprivate func setupBeInKnow() {
        let font1 = UIFont(name: "Gotham-MK3-Book", size: 16.0)!
        let font2 = UIFont(name: "Gotham-MK3-Medium", size: 16.0)!
        let attrString = NSMutableAttributedString(string: local.searchReceiveTips, attributes: [NSAttributedString.Key.font: font1])
        let boldAtt = [NSAttributedString.Key.font: font2]
        let range = NSMakeRange(0, attrString.length)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 7.0
        style.alignment = .left
        attrString.addAttributes(boldAtt, range: local.searchReceiveTipsRange1)
        attrString.addAttributes(boldAtt, range: local.searchReceiveTipsRange2)
        attrString.addAttributes(boldAtt, range: local.searchReceiveTipsRange3)
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1.5, range: range)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: range)
        beInKnowLabel.attributedText = attrString
        beInKnowLabel.sizeToFit()
    }
    
    fileprivate func startIndicator() {
        activityIndicator.isHidden = false
        view.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
    }
    
    fileprivate func stopIndicator() {
        activityIndicator.isHidden = true
        view.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
    }

    fileprivate func setBackgroundTransparency() {
        
        requiredLabel.textColor = UIColor.gray
        signupTV.backgroundColor = UIColor.white
        disclaimerTV.backgroundColor = UIColor.white
        signupTV.layer.cornerRadius = 5.0
        signupTV.clipsToBounds = true
        disclaimerTV.layer.cornerRadius = 5.0
        disclaimerTV.clipsToBounds = true
        
        // For Testing Purposes
//        signupTV.layer.borderColor = color
//        signupTV.layer.borderWidth = borderWidth
//        disclaimerTV.layer.borderColor = color
//        disclaimerTV.layer.borderWidth = borderWidth
    }
    
    fileprivate func secureTextFields() {
        countryTF.isSecureTextEntry = true
        emailTF.isSecureTextEntry = true
        firstNameTF.isSecureTextEntry = true
        lastNameTF.isSecureTextEntry = true
        phoneNumberTF.isSecureTextEntry = true
        genderTF.isSecureTextEntry = true
        birthdateTF.isSecureTextEntry = true
    }
    
    fileprivate func setupLabelTextsAndSizes() {
        
        country = MKCountry.countries.first!
        emailTF.text = email
        countryTF.text = country.name
        signupButton.setTitle(local.signupButton.uppercased(), for: .normal)
        requiredLabel.text = local.requiredText
        neverMissABeatLabel.text = local.signupNeverMissABeat
        
        countryLabel.text = local.signupCountryPlaceholder
        phoneNumberLabel.text = local.signupPhoneNumberPlaceholder
        firstNameLabel.text = local.signupFirstNamePlaceholder
        lastNameLabel.text = local.signupLastNamePlaceholder
        emailLabel.text = local.signupEmailPlaceholder
        genderLabel.text = local.signupGenderPlaceholder
        birthdateLabel.text = local.signupBirthdayPlaceholder
        signupTV.text = local.signupSignMeUpText
        genderTF.placeholder = local.chooseOne
        birthdateTF.placeholder = local.chooseOne
        languageButton.setTitle(MKLanguage.getSaved().nativeName.uppercased(), for: .normal)
        languageButton.sizeToFit()
        selectAStoreButton.setTitle(local.storesSelectAStore.uppercased(), for: .normal)
        
        addToggleFields()
    }
    
    fileprivate func addToggleFields() {
        
        // define fonts
        let font1 = UIFont(name: "Gotham-MK3-Book", size: 12.0)!
        let font2 = UIFont(name: "Gotham-MK3-Bold", size: 12.0)!
        
        // Collection and Men Toggle
        if local.addMenAndCollectionFields {
            let attrString1 = NSMutableAttributedString(string: local.collectionString, attributes: [NSAttributedString.Key.font: font1])
            let range1 = NSMakeRange(0, attrString1.length)
            attrString1.addAttributes([NSAttributedString.Key.font: font1], range: range1)
            collectionTV.attributedText = attrString1
            collectionToggle.isHidden = false
            collectionTV.isHidden = false
            collectionToggleHeight.constant = 44
            collectionTVHeight.constant = 31
            collectionSpacer.constant = 0
            
            let attrString2 = NSMutableAttributedString(string: local.menString, attributes: [NSAttributedString.Key.font: font1])
            let range2 = NSMakeRange(0, attrString2.length)
            attrString2.addAttributes([NSAttributedString.Key.font: font1], range: range2)
            menTV.attributedText = attrString2
            menToggle.isHidden = false
            menTV.isHidden = false
            menToggleHeight.constant = 44
            menTVHeight.constant = 31
            menSpacer.constant = 0
        } else {
            collectionToggle.isOn = false
            collectionToggle.isHidden = true
            collectionTV.isHidden = true
            collectionToggleHeight.constant = 0
            collectionTVHeight.constant = 0
            collectionSpacer.constant = 0
            
            menToggle.isOn = false
            menToggle.isHidden = true
            menTV.isHidden = true
            menToggleHeight.constant = 0
            menTVHeight.constant = 0
            menSpacer.constant = 0
        }
        
        
        // KorVIP Toggle fields
        if local.addKorsVIPFields {
            let attString3 = NSMutableAttributedString(string: local.signupVIP, attributes: [NSAttributedString.Key.font: font1])
            attString3.addAttributes([NSAttributedString.Key.font: font2], range: local.signupVIPRange)
            vipTV.attributedText = attString3
            vipToggle.isHidden = false
            vipTV.isHidden = false
            vipToggleHeight.constant = 44
            vipTVHeight.constant = 31
            vipSpacer.constant = 0
        } else {
            vipToggle.isOn = false
            vipToggle.isHidden = true
            vipTV.isHidden = true
            vipToggleHeight.constant = 0
            vipTVHeight.constant = 0
            vipSpacer.constant = 0
        }
    }
    
    fileprivate func setupTextView(textView: UITextView, string: String, font: UIFont, style: NSMutableParagraphStyle) {
        let attrString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
        let range = NSMakeRange(0, attrString.length)
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: range)
        textView.attributedText = attrString
        textView.sizeToFit()
    }
    
    fileprivate func setupToggleViews() {
        // set style and font
        let font = UIFont(name: "Gotham-MK3-Book", size: 12.0)!
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 7.0
        style.alignment = .left
        
        // setup Toggle Views
        setupTextView(textView: hearAboutTV, string: local.hearAbout, font: font, style: style)
        setupTextView(textView: signupTV, string: local.signupSignMeUpText, font: font, style: style)
    }
    
    fileprivate func setupFormats() {
        requiredLabel.format2()
        countryLabel.format1()
        phoneNumberLabel.format1()
        firstNameLabel.format1()
        lastNameLabel.format1()
        emailLabel.format1()
        genderLabel.format1()
        birthdateLabel.format1()
        languageButton.format1()
        selectAStoreButton.format1()
        neverMissABeatLabel.format4()
        let gray = UIColor(displayP3Red: 0.44, green: 0.44, blue: 0.44, alpha: 1.0)
        countryTF.format1(color: UIColor.black, borderColorBlack: false, noFonts: false)
        phoneNumberTF.format1(color: UIColor.black, borderColorBlack: false, noFonts: false)
        firstNameTF.format1(color: UIColor.black, borderColorBlack: false, noFonts: false)
        lastNameTF.format1(color: UIColor.black, borderColorBlack: false, noFonts: false)
        emailTF.format1(color: UIColor.black, borderColorBlack: false, noFonts: false)
        genderTF.format1(color: gray, borderColorBlack: false, noFonts: false)
        birthdateTF.format1(color: gray, borderColorBlack: false, noFonts: false)
    }
    
    fileprivate func observeTextFields() {
        
        // set delegates
        countryTF.delegate = self
        firstNameTF.delegate = self
        lastNameTF.delegate = self
        emailTF.delegate = self
        genderTF.delegate = self
        phoneNumberTF.delegate = self
        birthdateTF.delegate = self
        
        // set targets for beginning to edit
        firstNameTF.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        lastNameTF.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        emailTF.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        phoneNumberTF.addTarget(self, action: #selector(textFieldDidBegin(_:)), for: .editingDidBegin)
        
        // set targets for editing
        firstNameTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        lastNameTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        emailTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        phoneNumberTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // set targets for ending edits
        firstNameTF.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        lastNameTF.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        emailTF.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        phoneNumberTF.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
    }
    
    fileprivate func setupDisclaimer() {
        let font = UIFont(name: "Gotham-MK3-Book", size: 12.0)!
        let font2 = UIFont(name: "Gotham-MK3-Bold", size: 12.0)!
        let attrString = NSMutableAttributedString(string: local.signupDisclaimer, attributes: [NSAttributedString.Key.font:font])
        let range = NSMakeRange(0, attrString.length)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 7.0
        style.alignment = .left
        attrString.addAttribute(NSAttributedString.Key.link, value: local.signupDisclaimerTerms, range: local.signupDisclaimerRange1 )
        attrString.addAttribute(NSAttributedString.Key.link, value: local.signupDisclaimerPrivacy, range: local.signupDisclaimerRange2 )
        attrString.addAttributes([NSAttributedString.Key.font: font2], range: local.signupDisclaimerRange3)
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: range)
        disclaimerTV.attributedText = attrString
        disclaimerTV.delegate = self
        disclaimerTV.linkTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([NSAttributedString.Key.foregroundColor.rawValue: UIColor.black, NSAttributedString.Key.underlineStyle.rawValue: 1])
        disclaimerTV.sizeToFit()
    }
    
    fileprivate func clearFields() {
        emailTF.text = ""
        firstNameTF.text = ""
        lastNameTF.text = ""
        phoneNumberTF.text = ""
        genderTF.text = ""
        countryTF.text = country.name
        birthdateTF.text = ""
        let gray = UIColor(displayP3Red: 0.44, green: 0.44, blue: 0.44, alpha: 1.0)
        genderTF.format1(color: gray, borderColorBlack: false, noFonts: false)
        birthdateTF.format1(color: gray, borderColorBlack: false, noFonts: false)
    }
    
    fileprivate var isReadyForSignup: Bool {
        
        if !signupToggle.isOn {
            let alert = UIAlertController(title: local.alert, message: local.alertSignup, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        if firstNameTF.text == nil || firstNameTF.text == "" {
            let alert = UIAlertController(title: local.alert, message: local.alertFirstName, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        if lastNameTF.text == nil || lastNameTF.text == "" {
            let alert = UIAlertController(title: local.alert, message: local.alertLastName, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        if emailTF.text == nil || !emailTF.text!.isValidEmail {
            let alert = UIAlertController(title: local.alert, message: local.alertEmailAddress, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    fileprivate func checkValidity(ofTextField textField: UITextField) {
        
        if textField.text == "" {
            return
        }
        
        if textField == emailTF  {
            if !textField.text!.isValidEmail {
                textField.text = ""
                let alert = UIAlertController(title: local.alert, message: local.alertEmailAddress, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        if textField == birthdateTF {
            if !textField.text!.isValidMonthDay {
                textField.text = ""
                let alert = UIAlertController(title: local.alert, message: local.alertBirthdate, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func setPopupTransitionDelegate() {
        
        let chromeColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4)
        let size = DVSize(wFPercent: 0.5, hFPercent: 0.361)
        let transitionSize = DVTransitionSize(start: size, displayed: size, end: size)
        let settings = DVTransitionSettings(position: .position(.center),
                                            directionIn: .position(.bottom),
                                            directionOut: .position(.bottom),
                                            fade: DVFade(fadeIn: false, fadeOut: false),
                                            speed: 0.3,
                                            chrome: chromeColor,
                                            entireScreen: true,
                                            chromeDismiss: true,
                                            size: transitionSize)
        
        popupTransitionDelegate = DVTransDelegate(settings: settings, viewController: self)
    }
    
    fileprivate func setSSTransitionDelegate() {
        
        let chromeColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4)
        let size = DVSize(wFPercent: 1.0, hFPercent: 1.0)
        let transitionSize = DVTransitionSize(start: size, displayed: size, end: size)
        let settings = DVTransitionSettings(position: .position(.center),
                                            directionIn: .position(.center),
                                            directionOut: .position(.center),
                                            fade: DVFade(fadeIn: true, fadeOut: true),
                                            speed: 0.3,
                                            chrome: chromeColor,
                                            entireScreen: true,
                                            chromeDismiss: true,
                                            size: transitionSize)
        
        ssTransitionDelegate = DVTransDelegate(settings: settings, viewController: self)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

extension MKSignupVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == birthdateTF {
            timer?.invalidate()
            setTimer()
            let sb = UIStoryboard(name: "Popup", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "MKDatePicker") as! MKDatePicker
            vc.delegate = self
            vc.modalPresentationStyle = .custom
            vc.transitioningDelegate = popupTransitionDelegate
            present(vc, animated: true) {}
            return false
        }
        
        if textField == genderTF {
            timer?.invalidate()
            setTimer()
            let sb = UIStoryboard(name: "Popup", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "MKGenderPicker") as! MKGenderPicker
            vc.delegate = self
            vc.modalPresentationStyle = .custom
            vc.transitioningDelegate = popupTransitionDelegate
            present(vc, animated: true) {}
            return false
        }
        
        if textField == countryTF {
            timer?.invalidate()
            setTimer()
            let sb = UIStoryboard(name: "Popup", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "MKCountryPicker") as! MKCountryPicker
            vc.delegate = self
            vc.modalPresentationStyle = .custom
            vc.transitioningDelegate = popupTransitionDelegate
            present(vc, animated: true) {}
            return false
        }
        
        if textField == emailTF {
            textField.keyboardType = .emailAddress
        }
        
        if textField == phoneNumberTF {
            textField.keyboardType = .phonePad
        }
        
        if textField == birthdateTF {
            textField.keyboardType = .numberPad
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        checkValidity(ofTextField: textField)
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        checkValidity(ofTextField: textField)
        textField.resignFirstResponder()
        let local = self.local
        
        if textField == emailTF {
            if emailTF.text != ""  {
                MKNetwork.shared.getCustomer(withEmailAddress: emailTF.text!) { [weak self] (results, error) in
                    self?.stopIndicator()
                    
                    if error != nil {
                        let alert = UIAlertController(title: local.alert, message: error!.localizedDescription, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
                        self?.present(alert, animated: true, completion: nil)
                        return
                    }
                    
                    if results.contains("faultstring") || results.contains("AxisFault") || results.contains("Access Denied") {
                        let alert = UIAlertController(title: local.alert, message: local.alertGenericServerError, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
                        self?.clearFields()
                        self?.present(alert, animated: true, completion: nil)
                        return
                    }
                    
                    if results.contains("EmailAddress") {
                        let email = self?.emailTF.text ?? ""
                        let local = MKLocalize(withEmail: email, language: MKLanguage.getSaved())
                        let alert = UIAlertController(title: local.alertGreetings, message: local.alertEmailExists, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
                        self?.clearFields()
                        self?.present(alert, animated: true, completion: nil)
                        return
                    }
                }
            }
        }
        return true
    }
}

extension MKSignupVC: MKLanguagePickerDelegate {
    
    func languagePicker(lp: MKLanguagePicker, didSelectLanguage: MKLanguage) {
        updateTextAndSizes()
    }
    
}

extension MKSignupVC: MKCountryPickerDelegate {
    
    func countryPicker(cp: MKCountryPicker, didSelectCountry: MKCountry) {
        countryTF.text = didSelectCountry.name
        country = didSelectCountry
        countryTF.format1(color: UIColor.black, borderColorBlack: false, noFonts: false)
    }
}

extension MKSignupVC: MKGenderPickerDelegate {
    
    func genderPicker(gp: MKGenderPicker, didSelectGender: MKGender) {
        gender = didSelectGender
        if didSelectGender == .male {
            genderTF.text = local.genderMale
        } else {
            genderTF.text = local.genderFemale
        }
        genderTF.format1(color: UIColor.black, borderColorBlack: false, noFonts: false)
    }
}

extension MKSignupVC: MKDatePickerDelegate {
    func datePicker(gp: MKDatePicker, didSelectDate: String) {
        birthdateTF.text = didSelectDate
        birthdateTF.format1(color: UIColor.black, borderColorBlack: false, noFonts: false)
    }
}

extension MKSignupVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MKWebVC") as! MKWebVC
        vc.url = URL.absoluteString.addHTTPs
        present(vc, animated: true) {}
        return false
    }
}

extension MKSignupVC: MKScreenSaverDelegate {
    
    func mkScreenSaverDidDismiss(saver: MKScreenSaver) {
        timer?.invalidate()
        setTimer()
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
