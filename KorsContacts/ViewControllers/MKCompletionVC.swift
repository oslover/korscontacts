//
//  MKCompletionVC.swift
//  KorsContacts
//
//  Created by David Vallas on 4/20/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

class MKCompletionVC: MKBaseViewController {
    
    
    @IBOutlet weak var thankyouLabel: UILabel!
    @IBOutlet weak var completionLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    var emailAddress: String = "unknown_email@unknownemail.com"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let local = MKLocalize(withEmail: emailAddress, language: MKLanguage.getSaved())
        doneButton.setTitle(local.completionDone.uppercased(), for: .normal)
        completionLabel.text = local.completionCongratulations
        thankyouLabel.text = local.thankyouText
        thankyouLabel.format4()
        completionLabel.format3(withEmail: emailAddress)
        completionLabel.sizeToFit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Completion-\(primaryStore.id)")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    @IBAction func didPressDone(_ sender: UIButton) {
        dismiss(animated: true) {}
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
