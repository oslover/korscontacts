//
//  MKCountryPicker.swift
//  KorsContacts
//
//  Created by David Vallas on 4/4/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

protocol MKCountryPickerDelegate: AnyObject {
    func countryPicker(cp: MKCountryPicker, didSelectCountry: MKCountry)
}

class MKCountryPicker: MKBaseViewController {
    
    weak var delegate: MKCountryPickerDelegate?
    
    fileprivate let countries = MKCountry.countries
    
    fileprivate var selectedCountry: MKCountry = MKCountry.countries.first!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var countryPicker: UIPickerView!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBAction func selectPressed(_ sender: UIButton) {
        delegate?.countryPicker(cp: self, didSelectCountry: selectedCountry)
        self.dismiss(animated: true) {
            // do nothing
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
        countryPicker.delegate = self
        countryPicker.dataSource = self
        titleLabel.text = local.signupCountryPlaceholder.removeAsterik
        titleLabel.sizeToFit()
        doneButton.setTitle(local.completionDone.uppercased(), for: .normal)
        view.shadow()
    }
}

extension MKCountryPicker: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
}

extension MKCountryPicker: UIPickerViewDelegate {
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCountry = countries[row]
    }
}
