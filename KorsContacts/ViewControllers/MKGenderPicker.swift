//
//  MKGenderPicker.swift
//  KorsContacts
//
//  Created by David Vallas on 4/12/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

protocol MKGenderPickerDelegate: AnyObject {
    func genderPicker(gp: MKGenderPicker, didSelectGender: MKGender)
}

class MKGenderPicker: MKBaseViewController {
    
    weak var delegate: MKGenderPickerDelegate?
    
    let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var genderPicker: UIPickerView!
    @IBOutlet weak var doneButton: UIButton!
    
    
    fileprivate var selectedGender = MKGender.male
    
    @IBAction func selectPressed(_ sender: UIButton) {
        delegate?.genderPicker(gp: self, didSelectGender: selectedGender)
        self.dismiss(animated: true) {
            // do nothing
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genderPicker.delegate = self
        genderPicker.dataSource = self
        view.shadow()
        let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
        doneButton.setTitle(local.completionDone.uppercased(), for: .normal)
        genderLabel.text = local.signupGenderPlaceholder
        genderLabel.sizeToFit()
    }
}

extension MKGenderPicker: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
}

extension MKGenderPicker: UIPickerViewDelegate {
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 { return local.genderMale }
        return local.genderFemale
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 { return selectedGender = .male }
        else { selectedGender = .female }
    }
}
