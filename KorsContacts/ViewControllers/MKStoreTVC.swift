//
//  MKStoreTVC.swift
//  KorsContacts
//
//  Created by David Vallas on 4/25/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit
import CoreLocation

protocol MKStoreTVCDelegate: AnyObject {
    func storeTVC(storeTVC: MKStoreTVC, didSelectStore: MKStore)
}

class MKStoreTVC: UITableViewController {
    
    var spinner: UIActivityIndicatorView!
    weak var delegate: MKStoreTVCDelegate?
    
    var location: CLLocation!

    fileprivate var local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
    
    var stores: [MKStore] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add shadows
        view.shadow()
        
        // Change highlight for UITableView Cells
        let highlightView = UIView()
        highlightView.backgroundColor = UIColor(displayP3Red: 0.956, green: 0.956, blue: 0.956, alpha: 1.0)
        UITableViewCell.appearance().selectedBackgroundView = highlightView
        
        // Set the tableviews vertical and horizontal indicators off
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        
        // add a refresh control to the tableview
        refreshControl?.addTarget(self, action: #selector(getStores), for: .valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getStores()
    }
    
    // get the stores
    @objc func getStores() {
        addSpinner()
        MKNetwork.shared.getStores(withLocation: location) { [weak self] (mkstores, error) in
            self?.stopSpinner()
            self?.refreshControl?.endRefreshing()
            if error != nil {
                let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
                let alert = UIAlertController(title: local.alert, message: error!.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
                self?.present(alert, animated: true, completion: nil)
            }
            
            self?.stores = mkstores
        }
    }
    
    fileprivate func addSpinner() {
        if spinner == nil {
            spinner = UIActivityIndicatorView(style: .gray)
            spinner.frame = CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
            spinner.center = CGPoint(x: tableView.bounds.width / 2.0, y: tableView.bounds.height / 2.0)
            tableView.addSubview(spinner)
        }
        spinner.isHidden = false
        spinner.startAnimating()
    }
    
    fileprivate func stopSpinner() {
        spinner.stopAnimating()
        spinner.isHidden = true
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95.0
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
        return stores.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MKStoreCell") as! MKStoreCell
        let store = stores[indexPath.row]
        cell.storeID.text = "\(local.storeID) \(store.id.uppercased())"
        cell.address.text = store.address.capitalized
        cell.address2.text = store.address2.capitalized
        cell.city.text = store.city.capitalized
        cell.storeID.format1()
        cell.address.format2()
        cell.address2.format2()
        cell.city.format2()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.storeTVC(storeTVC: self, didSelectStore: stores[indexPath.row])
    }
}
