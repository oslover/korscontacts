//
//  MKInitialVC.swift
//  KorsContacts
//
//  Created by David Vallas on 4/13/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookCore

class MKInitialVC: UIViewController {
    
    @IBOutlet weak var facebookHolder: UIButton!
    
    @IBOutlet weak var twitterHolder: UIButton!
    
    fileprivate var manager = FBSDKLoginManager()
    
    @IBAction func signupPressed(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MKSignupVC") as! MKSignupVC
        present(vc, animated: true) {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let location1 = facebookHolder.center
        let location2 = twitterHolder.center
        let fbLoginButton = LoginButton(readPermissions: [ .publicProfile, .email ])
        fbLoginButton.center = location1
        fbLoginButton.delegate = self
        view.addSubview(fbLoginButton)
        
        let twLoginButton = LoginButton(readPermissions: [ .publicProfile, .email ])
        twLoginButton.center = location2
        view.addSubview(twLoginButton)
        
        facebookHolder.removeFromSuperview()
        twitterHolder.removeFromSuperview()
        
        manager.loginBehavior = .web
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        logoutOfFacebook()
        
    }
    
    fileprivate func logoutOfFacebook() {
        manager.logOut()
        FBSDKAccessToken.setCurrent(nil)
        FBSDKProfile.setCurrent(nil)
        let deletepermission = FBSDKGraphRequest(graphPath: "me/permissions/", parameters: nil, httpMethod: "DELETE")
        let _ = deletepermission?.start(completionHandler: {(connection,result,error)-> Void in
            print("the delete permission is \(String(describing: result))")
        })
    }
}

extension MKInitialVC: LoginButtonDelegate {
    
    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        logoutOfFacebook()
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        
    }
    
}
