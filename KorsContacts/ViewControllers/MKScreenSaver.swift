//
//  MKScreenSaver.swift
//  KorsContacts
//
//  Created by David Vallas on 6/18/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import UIKit

protocol MKScreenSaverDelegate: AnyObject {
    func mkScreenSaverDidDismiss(saver: MKScreenSaver)
}

class MKScreenSaver: MKBaseViewController {

    
    @IBOutlet weak var mklabel: UIImageView!
    var timer: Timer?
    weak var delegate: MKScreenSaverDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let x = view.frame.width / 2.0 - mklabel.frame.width / 2.0
        let y = view.frame.height / 2.0 - mklabel.frame.height / 2.0
         mklabel.frame = CGRect(x: x,
                                y: y,
                                width: mklabel.frame.width,
                                height: mklabel.frame.height)
        // Design has asked not to reposition the text.
        // timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(repositionView), userInfo: nil, repeats: true)
    }
    
    deinit {
        timer?.invalidate()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        dismiss(animated: true) {}
        delegate?.mkScreenSaverDidDismiss(saver: self)
    }
    
    @objc fileprivate func repositionView() {
        let x: UInt32 = UInt32(view.frame.width) - UInt32(mklabel.frame.width)
        let y: UInt32 = UInt32(view.frame.height) - UInt32(mklabel.frame.height)
        let randomX = CGFloat(arc4random_uniform(x))
        let randomY = CGFloat(arc4random_uniform(y))
        mklabel.alpha = 0.0
        mklabel.frame = CGRect(x: randomX, y: randomY, width: mklabel.frame.width, height: mklabel.frame.height)
        UIView.animate(withDuration: 1.0, animations: {
            UIView.animate(withDuration: 1.0) {
                [weak self] in
                self?.mklabel.alpha = 0.0
            }
        }) { [weak self] (completed) in
            self?.mklabel.frame = CGRect(x: randomX, y: randomY, width: self!.mklabel.frame.width, height: self!.mklabel.frame.height)
            if completed {
                UIView.animate(withDuration: 1.0) {
                    [weak self] in
                    self?.mklabel.alpha = 1.0
                }
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
