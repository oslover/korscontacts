//
//  MKDatePicker.swift
//  KorsContacts
//
//  Created by David Vallas on 1/17/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import Foundation

protocol MKDatePickerDelegate: AnyObject {
    func datePicker(gp: MKDatePicker, didSelectDate: String)
}

class MKDatePicker: MKBaseViewController {
    
    weak var delegate: MKDatePickerDelegate?
    
    let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
    let capitalizedMonths = MKLocalize(withEmail: "", language: MKLanguage.getSaved()).capitalizedMonths
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var datePicker: UIPickerView!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBAction func selectPressed(_ sender: UIButton) {
        
        let month = datePicker.selectedRow(inComponent: 0) + 1
        let day = datePicker.selectedRow(inComponent: 1) + 1
        
        var mString = String(month)
        var dString = String(day)
        
        if month < 10 { mString = "0\(month)" }
        if day < 10 { dString = "0\(day)" }
        
        delegate?.datePicker(gp: self, didSelectDate: "\(mString)/\(dString)")
        self.dismiss(animated: true) {
            // do nothing
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.delegate = self
        datePicker.dataSource = self
        view.shadow()
        dateLabel.text = local.signupBirthdayPlaceholder.removeLastWord
        dateLabel.sizeToFit()
        doneButton.setTitle(local.completionDone.uppercased(), for: .normal)
    }
}

extension MKDatePicker: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 { return local.months.count }
        if component == 1 {
            let row = pickerView.selectedRow(inComponent: 0)
            let month = row + 1
            return month.daysForMonth.count
        }
        return 0
    }
}

extension MKDatePicker: UIPickerViewDelegate {
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 { return capitalizedMonths[row] }
        if component == 1 {
            let r = pickerView.selectedRow(inComponent: 0)
            let month = r + 1
            let day = month.daysForMonth[row]
            return day
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 { pickerView.reloadComponent(1) }
    }
}
