//
//  MKCountryPicker.swift
//  KorsContacts
//
//  Created by David Vallas on 4/4/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

protocol MKLanguagePickerDelegate: AnyObject {
    func languagePicker(lp: MKLanguagePicker, didSelectLanguage: MKLanguage)
}

class MKLanguagePicker: MKBaseViewController {
    
    weak var delegate: MKLanguagePickerDelegate?
    
    fileprivate let languages = MKLanguage.names
    fileprivate var selectedLanguage: MKLanguage = MKLanguage.names.first!
    
    @IBOutlet weak var languageText: UILabel!
    @IBOutlet weak var languagePicker: UIPickerView!
    @IBOutlet weak var doneButton: UIButton!
    
    
    @IBAction func selectPressed(_ sender: UIButton) {
        selectedLanguage.save()
        delegate?.languagePicker(lp: self, didSelectLanguage: selectedLanguage)
        self.dismiss(animated: true) {
           // do nothing
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
        doneButton.setTitle(local.completionDone.uppercased(), for: .normal)
        languagePicker.delegate = self
        languagePicker.dataSource = self
        languageText.text = local.languageText.uppercased()
        languageText.sizeToFit()
        view.shadow()
    }
    
}

extension MKLanguagePicker: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languages.count
    }
}

extension MKLanguagePicker: UIPickerViewDelegate {
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let language = languages[row]
        if language == .britishEnglish || language == .english {
            return language.rawValue
        }
        return "\(language.rawValue) (\(language.nativeName))"
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedLanguage = languages[row]
    }
}
