//
//  MKWebView.swift
//  KorsContacts
//
//  Created by David Vallas on 8/7/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import UIKit
import WebKit

class MKWebVC: MKBaseViewController {
    
    var url = "https://www.google.com"
    
    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var backButton: MKButton!
    
    fileprivate var spinner: UIActivityIndicatorView!
    
    @IBAction func didPressBack(_ sender: UIButton) {
        dismiss(animated: true) {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // correct Language of Back Button
        let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
        backButton.setTitle(local.signupBack.removeBackArrow.uppercased(), for: .normal)
        
        // set navigation delegate
        webview.navigationDelegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // load URL and add spinner
        let uRL = URL(string: url)!
        let request = URLRequest(url: uRL)
        webview.load(request)
        addSpinner()
    }
    
    fileprivate func addSpinner() {
        if spinner == nil {
            spinner = UIActivityIndicatorView(style: .gray)
            spinner.frame = CGRect(x: 0, y: 0, width: 80.0, height: 80.0)
            spinner.center = CGPoint(x: webview.bounds.width / 2.0, y: webview.bounds.height / 2.0)
            webview.addSubview(spinner)
        }
        spinner.isHidden = false
        spinner.startAnimating()
    }
    
    fileprivate func stopSpinner() {
        spinner.stopAnimating()
        spinner.isHidden = true
    }
}

extension MKWebVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopSpinner()
    }
}
