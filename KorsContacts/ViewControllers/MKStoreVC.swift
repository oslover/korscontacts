//
//  MKStoreVC.swift
//  KorsContacts
//
//  Created by David Vallas on 4/25/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit
import CoreLocation

class MKStoreVC: MKBaseViewController {
    
    @IBOutlet weak var selectLabel: UILabel!
    @IBOutlet weak var tapLabel: UILabel!
    @IBOutlet weak var languageButton: UIButton!
    
    let locManager = CLLocationManager()
    var currentLocation: CLLocation = CLLocation(latitude: 33.7490, longitude: -84.3880)
    fileprivate var viewTransitionDelegate: DVTransDelegate!
    weak var storeTVC: MKStoreTVC?
    
    @IBAction func didPressLanguage(_ sender: UIButton) {
        let sb = UIStoryboard(name: "Popup", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MKLanguagePicker") as! MKLanguagePicker
        vc.delegate = self
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = viewTransitionDelegate
        present(vc, animated: true) {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViewTransitionDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "StoreSelection")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        updateText()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        locManager.delegate = self
        locManager.requestWhenInUseAuthorization()
    }
    
    fileprivate func updateText() {
        let language = MKLanguage.getSaved()
        let local = MKLocalize(withEmail: "", language: language)
        selectLabel.text = local.storesSelectAStore
        tapLabel.text = local.storesTap
        languageButton.setTitle(language.nativeName.uppercased(), for: .normal)
        selectLabel.format4()
        tapLabel.format3()
        languageButton.format1()
    }
    
    @IBAction func didPressPickStore(_ sender: UIButton) {
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways {
            currentLocation = locManager.location ?? CLLocation(latitude: 33.7490, longitude: -84.3880)
        }
        
        let sb = UIStoryboard(name: "Store", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MKStoreTVC") as! MKStoreTVC
        vc.delegate = self
        vc.location = currentLocation
        vc.modalPresentationStyle = .custom
        vc.transitioningDelegate = viewTransitionDelegate
        present(vc, animated: true) {}
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination
        
        if let v = vc as? MKStoreTVC {
            
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == .authorizedAlways {
                currentLocation = locManager.location ?? CLLocation(latitude: 33.7490, longitude: -84.3880)
            }
            storeTVC = v
            v.delegate = self
            v.location = currentLocation
        }
        
    }
    
    fileprivate func setViewTransitionDelegate() {
        let chromeColor = UIColor(displayP3Red: 0.0, green: 0.0, blue: 0.0, alpha: 0.4)
        let size = DVSize(wFPercent: 0.5, hFPercent: 0.325)
        let transitionSize = DVTransitionSize(start: size, displayed: size, end: size)
        let settings = DVTransitionSettings(position: .position(.center),
                                            directionIn: .position(.bottom),
                                            directionOut: .position(.bottom),
                                            fade: DVFade(fadeIn: false, fadeOut: false),
                                            speed: 0.3,
                                            chrome: chromeColor,
                                            entireScreen: true,
                                            chromeDismiss: true,
                                            size: transitionSize)
        
        viewTransitionDelegate = DVTransDelegate(settings: settings, viewController: self)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

extension MKStoreVC: MKLanguagePickerDelegate {
    
    func languagePicker(lp: MKLanguagePicker, didSelectLanguage: MKLanguage) {
        updateText()
        storeTVC?.tableView.reloadData()
    }
}

extension MKStoreVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        // update location if authorized
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == .authorizedAlways {
            currentLocation = locManager.location ?? CLLocation(latitude: 33.7490, longitude: -84.3880)
        }
        
        // set the storetvc location and get the location based stores
        storeTVC?.location = currentLocation
        storeTVC?.getStores()
    }
    
}

extension MKStoreVC: MKStoreTVCDelegate {
    
    func storeTVC(storeTVC: MKStoreTVC, didSelectStore: MKStore) {
        primaryStore = didSelectStore
        storeTVC.dismiss(animated: true) { }
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MKSignupVC") as! MKSignupVC
        present(vc, animated: true) {}
    }
}
