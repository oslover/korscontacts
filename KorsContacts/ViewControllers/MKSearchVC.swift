//
//  MKSearchVC.swift
//  KorsContacts
//
//  Created by David Vallas on 4/4/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

class MKSearchVC: MKBaseViewController {
    
    @IBOutlet weak var searchEmail: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var neverMissBeatLabel: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var tipsLabel: UILabel!
    
    let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchEmail.delegate = self
        searchEmail.placeholder = local.searchEmailPlaceholder
        searchEmail.layer.borderColor = UIColor(displayP3Red: 0.851, green: 0.851, blue: 0.851, alpha: 1).cgColor
        searchEmail.layer.borderWidth = 1.0
        neverMissBeatLabel.text = local.signupNeverMissABeat
        signupButton.setTitle(local.signupButton, for: .normal)
        setupTipsLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopIndicator()
        
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Search-\(primaryStore.id)")
        
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    @IBAction func didPressSearch(_ sender: UIButton) {
        
        if searchEmail.text?.isValidEmail == true {
            startIndicator()
            MKNetwork.shared.getCustomer(withEmailAddress: searchEmail.text!) { [weak self] (result, error) in
                self?.stopIndicator()
                
                if error != nil {
                    let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
                    let alert = UIAlertController(title: local.alert, message: error!.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    return
                }
            }
        } else {
            displayError()
        }
    }
    
    fileprivate func setupTipsLabel() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 20.0
        paragraphStyle.alignment = .center
        let attrString = NSMutableAttributedString(string: local.searchReceiveTips, attributes: [NSAttributedString.Key.font:UIFont(
            name: "Gotham-MK3-Book",
            size: 17.0)!])
        let boldAtt = [NSAttributedString.Key.font:UIFont(
            name: "Gotham-MK3-Bold",
            size: 17.0)!]
        
        attrString.addAttributes(boldAtt, range: NSRange(location: 25, length: 26))
        attrString.addAttributes(boldAtt, range: NSRange(location: 81, length: 34))
        attrString.addAttributes(boldAtt, range: NSRange(location: 141, length: 16))
        
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
        tipsLabel.attributedText = attrString
    }
    
    fileprivate func startIndicator() {
        activityIndicator.isHidden = false
        view.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
    }
    
    fileprivate func stopIndicator() {
        activityIndicator.isHidden = true
        view.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
    }
    
    fileprivate func displayError() {
        let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
        let alert = UIAlertController(title: local.alert, message: local.alertEmailAddress, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: local.alertOK, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension MKSearchVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.keyboardType = .emailAddress
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
