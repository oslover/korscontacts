//
//  CheckButton.swift
//  KorsContacts
//
//  Created by David Vallas on 6/27/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import Foundation

class CheckButton: UIButton {
    
    var isOn: Bool {
        didSet {
            if isOn == true { self.setImage(#imageLiteral(resourceName: "checkOn"), for: .normal) }
            if isOn == false { self.setImage(#imageLiteral(resourceName: "checkOff"), for: .normal) }
        }
    }
    
    required init(coder decoder: NSCoder) {
        isOn = false
        super.init(coder: decoder)!
        setImage(#imageLiteral(resourceName: "checkOff"), for: .normal)
        addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
        imageEdgeInsets = UIEdgeInsets.init(top: 10, left: 1, bottom: 16, right: 25)
        //UIEdgeInsetsMake(<#T##top: CGFloat##CGFloat#>, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)
    }
    
    @objc fileprivate func buttonClicked() {
        if isOn { isOn = false }
        else { isOn = true }
    }
}
