//
//  MKLanguage.swift
//  KorsContacts
//
//  Created by David Vallas on 4/30/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import UIKit

enum MKLanguage: String {
    
    case english = "English (US)"
    case britishEnglish = "English (UK)"
    case czech = "Czech"
    case german = "German"
    case danish = "Danish"
    case spanish = "Spanish"
    case finnish = "Finnish"
    case french = "French"
    case hungarian = "Hungarian"
    case italian = "Italian"
    case japanese = "Japanese"
    case korean = "Korean"
    case lithuanian = "Lithuanian"
    case latvian = "Latvian"
    case dutch = "Dutch"
    case norwegian = "Norwegian"
    case polish = "Polish"
    case portuguese = "Portuguese"
    case romanian = "Romanian"
    case swedish = "Swedish"
    
    static var names: [MKLanguage] = [.english, .britishEnglish, .czech, .danish, .dutch, .finnish, .french, .german, .hungarian, .italian, .japanese, .korean, .latvian, .lithuanian, .norwegian, .polish, .portuguese, .romanian, .spanish, .swedish]
    
    var nativeName: String {
        switch self {
        case .english: return "English (US)"
        case .britishEnglish: return "English (UK)"
        case .czech: return "čeština"
        case .german: return "Deutsch"
        case .danish: return "dansk"
        case .spanish: return "español"
        case .finnish: return "suomi"
        case .french: return "français"
        case .hungarian: return "magyar"
        case .italian: return "italiano"
        case .japanese: return "日本語"
        case .korean: return "한국어"
        case .lithuanian: return "lietuvių kalba"
        case .latvian: return "latviešu valoda"
        case .dutch: return "Nederlands"
        case .norwegian: return "Norsk"
        case .polish: return "polski"
        case .portuguese: return "português"
        case .romanian: return "limba română"
        case .swedish: return "Svenska"
        }
    }
    
    var mkLanguageCode: String {
        switch self {
        case .english: return "ENG"
        case .britishEnglish: return "ENG"
        case .czech: return "CZE"
        case .german: return "GER"
        case .danish: return "DAN"
        case .spanish: return "SPA"
        case .finnish: return "FIN"
        case .french: return "FRE"
        case .hungarian: return "HUN"
        case .italian: return "ITN"
        case .japanese: return "JPN"
        case .korean: return "KOR"
        case .lithuanian: return "LIT"
        case .latvian: return "LAV"
        case .dutch: return "DUT"
        case .norwegian: return "NOR"
        case .polish: return "POL"
        case .portuguese: return "POR"
        case .romanian: return "RON"
        case .swedish: return "SWE"
        }
    }
    
    var regionCode: String {
        switch self {
        case .english: return "US"
        case .britishEnglish: return "GB"
        case .czech: return "CZ"
        case .german: return "DE"
        case .danish: return "DK"
        case .spanish: return "ES"
        case .finnish: return "FI"
        case .french: return "FR"
        case .hungarian: return "HU"
        case .italian: return "IT"
        case .japanese: return "JP"
        case .korean: return "KR"
        case .lithuanian: return "LT"
        case .latvian: return "LV"
        case .dutch: return "NL"
        case .norwegian: return "NO"
        case .polish: return "PL"
        case .portuguese: return "PT"
        case .romanian: return "RO"
        case .swedish: return "SE"
        }
    }
    
    var languageIdentifer: String {
        switch self {
        case .english: return "en-US"
        case .britishEnglish: return "en-GB"
        case .czech: return "cs-CZ"
        case .german: return "de"
        case .danish: return "da-DK"
        case .spanish: return "es"
        case .finnish: return "fi-FI"
        case .french: return "fr-FR"
        case .hungarian: return "hu-HU"
        case .italian: return "it"
        case .japanese: return "ja"
        case .korean: return "ko"
        case .lithuanian: return "lt-IT"
        case .latvian: return "lv"
        case .dutch: return "nl"
        case .norwegian: return "nn"
        case .polish: return "pl"
        case .portuguese: return "pt"
        case .romanian: return "ro"
        case .swedish: return "sv"
        }
    }
    
    
    func save()  {
        let defaults = UserDefaults.standard
        defaults.set(self.rawValue, forKey: "MKLanguage")
    }
    
    static func getSaved() -> MKLanguage {
        let defaults = UserDefaults.standard
        if let stringValue = defaults.string(forKey: "MKLanguage") {
            return MKLanguage(rawValue: stringValue) ?? .english
        }
        return .english
    }
    
}
