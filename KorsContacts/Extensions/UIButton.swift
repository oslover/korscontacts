//
//  UIButton.swift
//  KorsContacts
//
//  Created by David Vallas on 7/19/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import UIKit

extension UIButton {
    
    /// format used for the text uibuttons
    func format1() {
        
        // create attributed string
        let string = self.title(for: .normal) ?? ""
        let font = UIFont(name: "Gotham-MK3-Medium", size: 12.0)!
        let attrString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
        let range = NSMakeRange(0, attrString.length)
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
        attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
        setAttributedTitle(attrString, for: .normal)
    }
    
    
}


