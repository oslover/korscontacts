//
//  Date.swift
//  KorsContacts
//
//  Created by David Vallas on 5/8/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation


extension Date {
    
    var formatted: String {
        let format1 = DateFormatter()
        let format2 = DateFormatter()
        format1.dateFormat = "yyyy-MM-dd"
        format2.dateFormat = "hh:mm:ss"
        let string1 = format1.string(from: self)
        let string2 = format2.string(from: self)
        return string1 + "T" + string2
    }
}
