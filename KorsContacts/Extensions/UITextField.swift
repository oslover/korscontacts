//
//  UITextField.swift
//  KorsContacts
//
//  Created by David Vallas on 7/19/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import Foundation

extension UITextField {
    
    /// format used for the text uitextfield, the color is the text color that will be used
    func format1(color: UIColor, borderColorBlack: Bool, noFonts: Bool) {
        
        // add slight border to the left of the text view
        if leftView == nil {
            let rect = CGRect(x: 0, y: 0, width: 8, height: 5)
            let view = UIView(frame: rect)
            leftView = view
            leftViewMode = .always
        }
        
        // set the place holder color
        if placeholder != nil {
            let gray = UIColor(displayP3Red: 0.44, green: 0.44, blue: 0.44, alpha: 1.0)
            attributedPlaceholder = NSAttributedString(string: placeholder!, attributes: [NSAttributedString.Key.foregroundColor: gray])
        }
        
        // format border and colors
        let c = borderColorBlack == true ? UIColor.black.cgColor : UIColor(displayP3Red: 0.82, green: 0.82, blue: 0.82, alpha: 1.0).cgColor
        layer.borderColor = c
        layer.borderWidth = 1.0
        backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        if text != nil && noFonts == false {
            // format text
            textColor = color
            let font = UIFont(name: "Gotham-MK3-Book", size: 12.0)!
            let attrString = NSMutableAttributedString(string: text!, attributes: [NSAttributedString.Key.font: font])
            let range = NSMakeRange(0, attrString.length)
            attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
            textAlignment = .justified
            attributedText = attrString
        }
    }
    
}
