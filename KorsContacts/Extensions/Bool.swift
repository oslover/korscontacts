//
//  Bool.swift
//  KorsContacts
//
//  Created by David Vallas on 4/21/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation

extension Bool {
    
    var stringValue: String {
        if self == true { return "1" }
        return "0"
    }
    
    
}
