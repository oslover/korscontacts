//
//  UILabel.swift
//  KorsContacts
//
//  Created by David Vallas on 7/19/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import Foundation

extension UILabel {
    
    /// format used for the text field labels and store IDs
    func format1() {
        let string = text ?? ""
        let font = UIFont(name: "Gotham-MK3-Medium", size: 12.0)!
        let attrString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
        let range = NSMakeRange(0, attrString.length)
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
        attributedText = attrString
    }
    
    /// format used for address and city labels
    func format2() {
        let string = text ?? ""
        let font = UIFont(name: "Gotham-MK3-Book", size: 12.0)!
        let attrString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
        let range = NSMakeRange(0, attrString.length)
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
        attributedText = attrString
    }
    
    /// format used for information labels
    func format3() {
        let string = text ?? ""
        let font = UIFont(name: "Gotham-MK3-Book", size: 16.0)!
        let attrString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
        let range = NSMakeRange(0, attrString.length)
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
        attributedText = attrString
    }
    
    /// format used for information labels email addresses
    func format3(withEmail: String) {
        let string = text ?? ""
        let font = UIFont(name: "Gotham-MK3-Book", size: 16.0)!
        let emailFont = UIFont(name: "Gotham-MK3-BookItalic", size: 16.0)!
        let attrString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 7.0
        style.alignment = .left
        let range = NSMakeRange(0, attrString.length)
        let emailRange = string.rangeOf(string: withEmail)
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
        attrString.addAttributes([NSAttributedString.Key.font: emailFont], range: emailRange)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: range)
        attributedText = attrString
    }
    
    /// format used for title labels
    func format4() {
        let string = text ?? ""
        let font = UIFont(name: "Gotham-MK3-Light", size: 32.0)!
        let attrString = NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font])
        let range = NSMakeRange(0, attrString.length)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 9.0
        style.alignment = .left
        attrString.addAttribute(NSAttributedString.Key.kern, value: 1, range: range)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: range)
        attributedText = attrString
        sizeToFit()
    }
}
