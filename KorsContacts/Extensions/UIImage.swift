//
//  UIImage.swift
//  KorsContacts
//
//  Created by David Vallas on 4/12/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

extension UIImage {
    
    var blur: UIImage {
        let imageToBlur = CIImage(image: self)
        let blurfilter = CIFilter(name: "CIGaussianBlur")
        //blurfilter?.setValue(5, forKey: kCIInputRadiusKey)
        blurfilter?.setValue(imageToBlur, forKey: "inputImage")
        let resultImage = blurfilter?.value(forKey: "outputImage") as! CIImage
        var blurredImage = UIImage(ciImage: resultImage)
        let rect = CGRect(x: 0, y: 0, width: imageToBlur!.extent.size.width, height: imageToBlur!.extent.size.height)
        let cropped = resultImage.cropped(to: rect)
        blurredImage = UIImage(ciImage: cropped)
        return blurredImage
    }
    
}
