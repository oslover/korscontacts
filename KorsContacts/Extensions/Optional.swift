//
//  Optional.swift
//  KorsContacts
//
//  Created by David Vallas on 4/21/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation

extension Optional {
    
    var bool: Bool {
        if let bool = self as? Bool { return bool }
        print("Error: unable to unwrap \(String(describing: self)) as bool, returning false")
        return false
    }
    
    var dict: [String:AnyObject] {
        if let dict = self as? [String:AnyObject] { return dict }
        print("Error: unable to unwrap \(String(describing: self)) as string, returning ''")
        return [:]
    }
    
    var string: String {
        if let string = self as? String { return string }
        print("Error: unable to unwrap \(String(describing: self)) as string, returning ''")
        return ""
    }
    
    var optionalString: String {
        if let string = self as? String { return string }
        return ""
    }
    
    var dateString: String {
        if let string = self as? String { return string.dateString }
        return "1900-01-01"
    }
    
    var gender: String {
        if let gender = self as? MKGender { return gender.saveGender }
        return "U"
    }
    
    var phoneNumber: String {
        if let p = self as? String {
            var phone = p.replacingOccurrences(of: "(", with: "")
            phone = phone.replacingOccurrences(of: ")", with: "")
            phone = phone.replacingOccurrences(of: "-", with: "")
            phone = phone.replacingOccurrences(of: " ", with: "")
            return phone
        }
        return "9999999999"
    }
}
