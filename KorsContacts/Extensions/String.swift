//
//  String.swift
//  KorsContacts
//
//  Created by David Vallas on 4/5/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation

extension String {
    
    subscript(_ range: NSRange) -> String {
        let start = self.index(self.startIndex, offsetBy: range.lowerBound)
        let end = self.index(self.startIndex, offsetBy: range.upperBound)
        let subString = self[start..<end]
        return String(subString)
    }
    
    var removeBackArrow: String {
        if self.hasPrefix("< ") { return String(self.dropFirst().dropFirst()) }
        if self.hasPrefix("<") { return String(self.dropFirst()) }
        return self
    }
    
    var addHTTPs: String {
        if self.hasPrefix("http://") {
            let string = self.dropFirst(7)
            return "https://" + string
        }
        if self.hasPrefix("https://") { return self }
        return "https://" + self
    }
    
    var removeLastWord: String {
        var components = self.components(separatedBy: " ")
        if components.count > 1 {
            components.removeLast()
            var newString = ""
            for c in components {
                newString = newString + c + " "
            }
            return String(newString.dropLast())
        }
        return self
    }
    
    func rangeOf(string: String) -> NSRange {
        return (self as NSString).range(of: string)
    }
    
    var removeAsterik: String {
        if self.last == "*" { return String(self.dropLast()) }
        return self
    }
    
    var customerNumber: String {
        return self.xmlValue(forTag: "a:CustomerNumber")
    }
    
    func xmlValue(forTag: String) -> String {
        let a1 = self.components(separatedBy: "<\(forTag)>")
        if a1.count == 1 { return "" }
        let a2 = a1.last!.components(separatedBy: "</\(forTag)>")
        if a2.count == 1 && a2.first!.contains("<") { return "" }
        return a2.first!
    }
    
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    var isValidMonthDay: Bool {
        
        if !self.contains("/") {
            return false
        }
        
        if self.count < 3 {
            return false
        }
        
        let components = self.components(separatedBy: "/")
        
        let month = components[0]
        let day = components[1]
        
        guard let mNumber = Int(month) else { return false }
        guard let dNumber = Int(day) else { return false }
        
        if mNumber < 1 || mNumber > 12 { return false }
        if dNumber < 1 || dNumber > 31 { return false }
        
        return true
    }
    
    var dateString: String {
        
        if !self.contains("/") {
            return "1904-01-01T00:00:00"
        }
        
        let components = self.components(separatedBy: "/")
        
        var month = components[0]
        var day = components[1]
        
        guard let mNumber = Int(month) else { return "1904-01-01T00:00:00" }
        guard let dNumber = Int(day) else { return "1904-01-01T00:00:00" }
        
        if mNumber < 10 {
           month = "0" + String(mNumber)
        }
        
        if dNumber < 10 {
            day = "0" + String(dNumber)
        }
        
        return "1904-" + month + "-" + day + "T00:00:00"
    }
    
    subscript (i: Int) -> String {
        
        if i < 0 {
            return ""
        }
        
        
        if i > self.count - 1 {
            return ""
        }
        
        let index = self.index(self.startIndex, offsetBy: i + 1)
        let char = self[..<index].last
        if let c = char {
            return String(c)
        }
        return ""
    }
    
    subscript (r: CountableClosedRange<Int>) -> String {
        let startIndex =  self.index(self.startIndex, offsetBy: r.lowerBound)
        let endIndex = self.index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        return String(self[startIndex...endIndex])
    }
}
