//
//  UIView.swift
//  KorsContacts
//
//  Created by David Vallas on 4/13/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

extension UIView {
    
    func corner() {
        self.layer.cornerRadius = self.frame.size.width / 8
        self.clipsToBounds = true
    }
    
    func shadow() {
        self.layer.masksToBounds = true
        self.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        self.layer.shadowRadius = 8
        self.layer.shadowOpacity = 0.5
    }
    
    func shadow2() {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 8
        self.layer.shadowOffset = CGSize(width: 5.0, height: 0.0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.5
    }
    
    func outline() {
        self.layer.borderColor = UIColor(displayP3Red: 0.0, green: 122.0 / 255.0, blue: 1.0, alpha: 1.0).cgColor
        self.layer.borderWidth = 2.0
    }
    
    func deOutline() {
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.borderWidth = 0.0
    }
}
