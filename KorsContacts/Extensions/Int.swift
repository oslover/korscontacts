//
//  Int.swift
//  KorsContacts
//
//  Created by David Vallas on 1/17/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import Foundation

extension Int {
    
    var daysForMonth: [String] {
        
        if self == 2 {
            return (1...29).map{"\($0)"}
        }
        
        if self == 9 || self == 4 || self == 6 || self == 11 {
            return (1...30).map{"\($0)"}
        }
        
        return (1...31).map{"\($0)"}
    }
    
}
