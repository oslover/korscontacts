//
//  MKButton.swift
//  KorsContacts
//
//  Created by David Vallas on 7/27/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import Foundation

/// This button properly shows accents in Title Label 
class MKButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        var frame = titleLabel!.frame
        frame.size.height = bounds.size.height
        frame.origin.y = titleEdgeInsets.top
        titleLabel?.frame = frame
    }
    
}
