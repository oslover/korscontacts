//
//  MKTextView.swift
//  KorsContacts
//
//  Created by David Vallas on 8/7/18.
//  Copyright © 2018 David Vallas. All rights reserved.
//

import Foundation

protocol MKTextViewDelegate: class {
    func tappedLink()
}

class MKTextView: UITextView {
    
    var linkDetectDelegate: MKTextViewDelegate?
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let glyphIndex: Int? = layoutManager.glyphIndex(for: point, in: textContainer, fractionOfDistanceThroughGlyph: nil)
        let index: Int? = layoutManager.characterIndexForGlyph(at: glyphIndex ?? 0)
        if let characterIndex = index {
            if characterIndex < textStorage.length {
//                if textStorage.attribute(NSLinkAttributeName, at: characterIndex, effectiveRange: nil) != nil
                if textStorage.attribute(.link, at: characterIndex, effectiveRange: nil) != nil {
                    linkDetectDelegate?.tappedLink()
                    return self
                }
            }
        }
        
        return nil
    }
}
