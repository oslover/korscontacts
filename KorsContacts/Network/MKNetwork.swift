//
//  MKNetwork.swift
//  KorsContacts
//
//  Created by David Vallas on 4/4/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import Foundation
import CoreLocation

class MKNetwork: NSObject {
    
    static let shared = MKNetwork()
    
    let production = true
    
    var url: URL {
        if production { return URL(string: "https://api.michaelkors.com/ws/crmsoap/1.0")! }
        else { return URL(string: "https://apiuat.michaelkors.com/ws/crmsoap/1.1")! }
    }
    
    var vipURL: String {
        if production { return "https://api.MichaelKors.com/gateway/loyalty/1.0/loyaltyenroll" }
        else { return "https://apiuat.MichaelKors.com/gateway/loyalty/1.0/loyaltyenroll" }
    }
    
    var vipKey: String {
        if production { return "DONT KNOW" }
        else { return "3a6b5234-536e-4e29-b573-e32c1f9ad351" }
    }
    
    var apiKey: String {
        if production { return "aa75517f-923c-4dd3-9dae-da1c99a8210b" }
        else { return "add8787e-ddfe-4a22-bf12-cc70b091fc41" }
    }
    
    var divisionID: String {
        if production { return "133" }
        else { return "133" }
    }
    
    let acceptCertificationWithoutValidation = false
    
    func setVIP( withCustomer c: MKCustomer, completion: @escaping (_ result: Dictionary<String, AnyObject>?, _ error: Error?) -> Void) {
        
        let urlString = "\(vipURL)?email=\(c.email)&external_customer_id=\(c.customerid)&first_name=\(c.firstName)&last_name=\(c.lastName)&custom_attributes[gender]=\(c.gender)&birthdate=\(c.birthdate)"
        let url = URL(string: urlString)!
        let theRequest = NSMutableURLRequest(url: url)
        theRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        theRequest.addValue(vipKey, forHTTPHeaderField: "x-Gateway-APIKey")
        theRequest.httpMethod = "GET"
        theRequest.timeoutInterval = 5.0
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        
        session.dataTask(with: theRequest as URLRequest) { (data, response, error) in
            
            if error == nil {
                if let d = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: d, options: [.allowFragments]) as? Dictionary<String, AnyObject> ?? [:]
                        let success = json["success"].bool
                        if success {
                            DispatchQueue.main.async {
                                completion(json, nil)
                            }
                        }
                        return
                    } catch  {
                        let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
                        let err = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey: local.alertGenericServerError])
                        DispatchQueue.main.async {
                            completion(nil, err)
                        }
                        return
                    }
                }
            }
            
            var err = error
            
            if err == nil {
                let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
                err = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey: local.alertGenericServerError])
            }
            
            DispatchQueue.main.async {
                completion(nil, err)
            }
            
        }.resume()
    }
    
    func getCustomer(withEmailAddress: String, completion: @escaping (_ result: String, _ error: Error?) -> Void) {
        
        let soapMessage = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns='http://epicor.com/retail/CRM/7.0.0/' xmlns:epic='http://schemas.datacontract.org/2004/07/Epicor.Retail.Crm.CustomerWebService'><soapenv:Header/><soapenv:Body><ns:FindCustomers><ns:parameters><epic:StoreNumber>909</epic:StoreNumber></ns:parameters><ns:emailAddress>\(withEmailAddress)</ns:emailAddress></ns:FindCustomers></soapenv:Body></soapenv:Envelope>"
        
        let messageLength = String(describing: soapMessage.count)
        let theRequest = NSMutableURLRequest(url: url)
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue(messageLength, forHTTPHeaderField: "Content-Length")
        theRequest.addValue("http://epicor.com/retail/CRM/7.0.0/ICrmService/FindCustomers", forHTTPHeaderField: "SOAPAction")
        theRequest.addValue(apiKey, forHTTPHeaderField: "x-Gateway-APIKey")
        theRequest.addValue("MK_a6rf42", forHTTPHeaderField: "Partner-ID")
        theRequest.httpMethod = "POST"
        theRequest.timeoutInterval = 5.0
        theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        session.dataTask(with: theRequest as URLRequest) { (data, response, error) in
            
            if error == nil {
                
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8) {
                    DispatchQueue.main.async {
                        completion(result, nil)
                    }
                } else {
                    let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
                    let err = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey: local.alertGenericServerError])
                    DispatchQueue.main.async {
                        completion("", err)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion("", error)
                }
            }
            }.resume()
    }
    
    func saveCustomer(withUserInfo u: MKUserInfo, completion: @escaping (_ result: String, _ error: Error?) -> Void) {
        
        let regionCode = NSLocale.current.regionCode
        
        var isDoubleEmailOptIn: Bool {
            guard let r = regionCode else { return false }
            if r.contains("AT") { return true } // austria
            if r.contains("DK") { return true } // denmark
            if r.contains("DE") { return true } // germany
            if r.contains("LT") { return true } // lithuania
            if r.contains("LU") { return true } // luxembourg
            if r.contains("NO") { return true } // norway
            return false
        }
        
        let primaryPhoneDate = u.phoneNumber == nil ? "1904-01-01T00:00:00" : Date().formatted
        
        let emailFlag = isDoubleEmailOptIn == true ? "0" : "1"
        let languageCode = MKLanguage.getSaved().mkLanguageCode
        
        let xmlBeginning = "<?xml version='1.0' encoding='UTF-8'?><soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:epic='http://schemas.datacontract.org/2004/07/Epicor.Retail.Crm.CustomerWebService' xmlns:ns='http://epicor.com/retail/CRM/7.0.0/'><soapenv:Header /><soapenv:Body><ns:SaveNewCustomer>"
        
        let customer = "<ns:parameters><epic:CustomerNumber>-1</epic:CustomerNumber><epic:DatabaseGroupID>1</epic:DatabaseGroupID><epic:LanguageID>1033</epic:LanguageID><epic:SalesAssociateNumber>9999</epic:SalesAssociateNumber><epic:StoreNumber>\(primaryStore.id)</epic:StoreNumber></ns:parameters><ns:checkDuplicates>false</ns:checkDuplicates><ns:customer><epic:BirthDate>\(u.birthdate.dateString)</epic:BirthDate><epic:CreateDate>\(Date().formatted)</epic:CreateDate><epic:CreateStoreNumber>\(primaryStore.id)</epic:CreateStoreNumber><epic:CreateUser>11111</epic:CreateUser><epic:CreatedSource>DataApp</epic:CreatedSource><epic:CustomerDivisions><epic:CustomerDivisionWCF><epic:CreateDate>\(Date().formatted)</epic:CreateDate><epic:CreateSource>DataApp</epic:CreateSource><epic:CreateStoreNumber>\(primaryStore.id)</epic:CreateStoreNumber><epic:CreateUser>DataApp</epic:CreateUser><epic:DivisionID>\(divisionID)</epic:DivisionID><epic:ModifyDate>\(Date().formatted)</epic:ModifyDate><epic:ModifyStoreNumber>\(primaryStore.id)</epic:ModifyStoreNumber><epic:ModifyUser>DataApp</epic:ModifyUser><epic:NumberOfMailings>-1</epic:NumberOfMailings><epic:PrimaryAddressDate>\(Date().formatted)</epic:PrimaryAddressDate><epic:PrimaryAddressID>-1</epic:PrimaryAddressID><epic:PrimaryEmailDate>\(Date().formatted)</epic:PrimaryEmailDate><epic:PrimaryEmailID>-1</epic:PrimaryEmailID><epic:PrimaryPhoneDate>\(primaryPhoneDate)</epic:PrimaryPhoneDate><epic:PrimaryPhoneID>-1</epic:PrimaryPhoneID><epic:RecordState>Added</epic:RecordState><epic:SalesAssociateNumber>9999</epic:SalesAssociateNumber><epic:StoreNumber>\(primaryStore.id)</epic:StoreNumber></epic:CustomerDivisionWCF></epic:CustomerDivisions><epic:DistributionStatusCode>0</epic:DistributionStatusCode><epic:FirstName>\(u.firstName)</epic:FirstName><epic:Gender>\(u.gender.gender)</epic:Gender><epic:ID>-1</epic:ID><epic:LanguageCode>\(languageCode)</epic:LanguageCode><epic:LastName>\(u.lastName)</epic:LastName><epic:LastUpdateDate>\(Date().formatted)</epic:LastUpdateDate><epic:LastUpdateStoreNumber>\(primaryStore.id)</epic:LastUpdateStoreNumber><epic:LastUpdateUser>DataApp</epic:LastUpdateUser><epic:MaritalStatus>U</epic:MaritalStatus><epic:Number>-1</epic:Number><epic:RecordState>Added</epic:RecordState><epic:SalesAssociateNumber>\(primaryStore.id)</epic:SalesAssociateNumber><epic:StatusCode>A</epic:StatusCode><epic:StoreNumber>\(primaryStore.id)</epic:StoreNumber></ns:customer>"
        
        let phone = u.phoneNumber == nil ? "" : "<ns:customerPhones><epic:CustomerNumber>-1</epic:CustomerNumber><epic:Items><epic:CustomerPhoneWCF><epic:CountryCode>\(u.country.iso3Code)</epic:CountryCode><epic:CreateDate>\(Date().formatted)</epic:CreateDate><epic:CreateStoreNumber>9999</epic:CreateStoreNumber><epic:CreateUser>DataApp</epic:CreateUser><epic:DateLastModified>\(Date().formatted)</epic:DateLastModified><epic:ID>-1</epic:ID><epic:ModifyStoreNumber>9999</epic:ModifyStoreNumber><epic:ModifyUser>DataApp</epic:ModifyUser><epic:PhoneDivisions><epic:CustomerPhoneDivisionWCF><epic:DivisionID>\(divisionID)</epic:DivisionID><epic:ModifyDate>\(Date().formatted)</epic:ModifyDate><epic:ModifyUser>DataApp</epic:ModifyUser><epic:PhoneOptInDate>\(Date().formatted)</epic:PhoneOptInDate><epic:PhoneOptInFlagCode>\(emailFlag)</epic:PhoneOptInFlagCode><epic:RecordState>Added</epic:RecordState><epic:TextOptInDate>\(Date().formatted)</epic:TextOptInDate><epic:TextOptInFlagCode>0</epic:TextOptInFlagCode></epic:CustomerPhoneDivisionWCF></epic:PhoneDivisions><epic:PhoneExtensionNumber /><epic:PhoneIndicatorID>9</epic:PhoneIndicatorID><epic:PhoneNumber>\(u.phoneNumber.phoneNumber)</epic:PhoneNumber><epic:PhoneTypeCode>MOBI</epic:PhoneTypeCode><epic:RecordState>Added</epic:RecordState></epic:CustomerPhoneWCF></epic:Items></ns:customerPhones>"
        
        let countryCode = "<ns:customerSegmentation><epic:CustomerNumber>-1</epic:CustomerNumber><epic:CustomerSegmentationCodes><epic:CustomerSegmentationCodeWCF><epic:Code>\(u.country.iso3Code)</epic:Code><epic:GroupCode>C</epic:GroupCode><epic:OriginalGroupCode>C</epic:OriginalGroupCode><epic:RecordState>Added</epic:RecordState></epic:CustomerSegmentationCodeWCF></epic:CustomerSegmentationCodes></ns:customerSegmentation>"
        
        let email = "<ns:customerEmails><epic:CustomerNumber>-1</epic:CustomerNumber><epic:Items><epic:CustomerEmailWCF><epic:CreateDate>\(Date().formatted)</epic:CreateDate><epic:CreateStoreNumber>\(primaryStore.id)</epic:CreateStoreNumber><epic:CreateUser>DataApp</epic:CreateUser><epic:DateLastModified>\(Date().formatted)</epic:DateLastModified><epic:EmailAddress>\(u.email)</epic:EmailAddress><epic:EmailDivisions><epic:CustomerEmailDivisionWCF><epic:DivisionID>\(divisionID)</epic:DivisionID><epic:EmailID>-1</epic:EmailID><epic:EmailOptInDate>\(Date().formatted)</epic:EmailOptInDate><epic:EmailOptInFlagCode>\(emailFlag)</epic:EmailOptInFlagCode><epic:ModifyDate>\(Date().formatted)</epic:ModifyDate><epic:ModifyUser>DataApp</epic:ModifyUser><epic:RecordState>Added</epic:RecordState></epic:CustomerEmailDivisionWCF></epic:EmailDivisions><epic:EmailIndicatorID>9</epic:EmailIndicatorID><epic:EmailTypeCode>HOME</epic:EmailTypeCode><epic:ID>-1</epic:ID><epic:ModifyStoreNumber>\(primaryStore.id)</epic:ModifyStoreNumber><epic:ModifyUser>DataApp</epic:ModifyUser><epic:RecordState>Added</epic:RecordState></epic:CustomerEmailWCF></epic:Items></ns:customerEmails>"
        
        let collection = u.collection ? "<epic:CustomerAttributeWCF><epic:Code>PCOL</epic:Code><epic:Comment/><epic:Date>\(Date().formatted)</epic:Date><epic:DivisionID>\(divisionID)</epic:DivisionID><epic:GroupCode>PCOL</epic:GroupCode><epic:ModifyDate>\(Date().formatted)</epic:ModifyDate><epic:ModifyUser>110132</epic:ModifyUser><epic:OriginalCode>PCOL</epic:OriginalCode><epic:OriginalGroupCode>PCOL</epic:OriginalGroupCode><epic:RecordState>Added</epic:RecordState></epic:CustomerAttributeWCF>" : ""
        
        let men = u.men ? "<epic:CustomerAttributeWCF><epic:Code>PMEN</epic:Code><epic:Comment/><epic:Date>\(Date().formatted)</epic:Date><epic:DivisionID>\(divisionID)</epic:DivisionID><epic:GroupCode>PMEN</epic:GroupCode><epic:ModifyDate>\(Date().formatted)</epic:ModifyDate><epic:ModifyUser>110132</epic:ModifyUser><epic:OriginalCode>PMEN</epic:OriginalCode><epic:OriginalGroupCode>PMEN</epic:OriginalGroupCode><epic:RecordState>Added</epic:RecordState></epic:CustomerAttributeWCF>" : ""
        
        let xmlEnding = "</ns:SaveNewCustomer></soapenv:Body></soapenv:Envelope>"
        
        let soapMessage = xmlBeginning + customer + phone + email + countryCode + collection + men + xmlEnding
        
        let messageLength = String(describing: soapMessage.count)
        let theRequest = NSMutableURLRequest(url: url)
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue(messageLength, forHTTPHeaderField: "Content-Length")
        theRequest.addValue("http://epicor.com/retail/CRM/7.0.0/ICrmService/SaveNewCustomer", forHTTPHeaderField: "SOAPAction")
        theRequest.addValue(apiKey, forHTTPHeaderField: "x-Gateway-APIKey")
        theRequest.addValue("MK_a6rf42", forHTTPHeaderField: "Partner-ID")
        theRequest.httpMethod = "POST"
        theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        session.dataTask(with: theRequest as URLRequest) { (data, response, error) in
            if error == nil {
                
                if let data = data, let result = String(data: data, encoding: String.Encoding.utf8) {
                    
                    var err: Error? = nil
                    let local = MKLocalize(withEmail: "", language: MKLanguage.getSaved())
                    
                    // soap message returns length error for phone number, give appropriate error message
                    if result.contains("Phone number length did not meet requirements for chosen country") {
                        err = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey: local.alertPhoneNumber])
                    }
                        
                        // soap message returns an date parsing error, give appropriate error message
                    else if result.contains("cannot be parsed as the type \'DateTime") {
                        err = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey: local.alertBirthdate])
                    }
                        
                        // phone number had invalid characters, create an error
                    else if result.contains("Invalid character(s) in phone number") {
                        err = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey: local.alertPhoneNumber])
                    }
                        
                        // give a generic customer error, we aren't sure of other cases that would cause a failure, SOAP sucks
                    else if !result.contains("<a:CustomerNumber>") {
                        err = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey: local.alertSaveError])
                    }
                    
                    // we have a valid soap response, a customer was made
                    
                    DispatchQueue.main.async {
                        if let e = err {
                            completion("", e)
                        } else {
                            completion(result, nil)
                        }
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion("", error)
                }
            }
            }.resume()
    }
    
    func getStores(withLocation l: CLLocation, completion: @escaping (_ result: [MKStore], _ error: Error?) -> Void) {
        
        let url = "https://liveapi.yext.com/v2/accounts/me/locations/geosearch?api_key=67d1a677e9a75d44877e71441234153b&v=20170401&limit=50&radius=100&location=\(l.coordinate.latitude),\(l.coordinate.longitude)"
        let request = NSMutableURLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if error == nil {
                if let d = data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: d, options: []) as? Dictionary<String, AnyObject> ?? [:]
                        let stores = MKStore.parseJSON(json: json)
                        DispatchQueue.main.async {
                            completion(stores, nil)
                        }
                    } catch {
                        DispatchQueue.main.async {
                            completion([], nil)
                            print("Error: no JSON return from getStores request")
                        }
                    }
                } else {
                    
                }
            } else {
                DispatchQueue.main.async {
                    completion([], error)
                }
            }
            }.resume()
    }
}

extension MKNetwork: URLSessionDelegate {
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        //accept all certs when testing, perform default handling otherwise
        if acceptCertificationWithoutValidation {
            print("Accepting cert as always")
            completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        } else {
            completionHandler(.performDefaultHandling, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        }
    }
}
