//
//  MKStoreCell.swift
//  KorsContacts
//
//  Created by David Vallas on 4/25/17.
//  Copyright © 2017 David Vallas. All rights reserved.
//

import UIKit

class MKStoreCell: UITableViewCell {
    
    @IBOutlet weak var storeID: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var address2: UILabel!
    @IBOutlet weak var city: UILabel!
    
}
